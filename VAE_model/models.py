import torch
from torch import nn, optim
from torch.nn import functional as F
from VAE_model.binary_activation import StochasticBinaryActivation

EPS = 1e-12


class VAE(nn.Module):
    def __init__(self, img_size, latent_spec, nb_filter_conv1=32, nb_filter_conv2=32, nb_filter_conv3=64,
                 nb_filter_conv4=64, nb_classes=None, is_classification=False, is_classification_random_continue=False,
                 binary_variational=False, filter_size=(4, 4), stride=2, temperature=.67):
        """
        Class which defines model and forward pass.
        Parameters
        ----------
        img_size : tuple of ints
            Size of images. E.g. (1, 32, 32) or (3, 64, 64).
        latent_spec : dict
            Specifies latent distribution. For example:
            {'cont': 10, 'disc': [10, 4, 3]} encodes 10 normal variables and
            3 gumbel softmax variables of dimension 10, 4 and 3. A latent spec
            can include both 'cont' and 'disc' or only 'cont' or only 'disc'.
        temperature : float
            Temperature for gumbel softmax distribution.

        """
        super(VAE, self).__init__()
        
        # Parameters
        self.is_classification = is_classification
        self.is_classification_random_continue = is_classification_random_continue
        self.binary_variational = binary_variational
        self.nb_classes = nb_classes
        self.img_size = img_size
        self.is_continuous = 'cont' in latent_spec
        self.is_discrete = 'disc' in latent_spec
        self.latent_spec = latent_spec
        self.filter_size = filter_size
        self.stride = stride
        self.num_pixels = img_size[1] * img_size[2]
        self.temperature = temperature
        self.hidden_dim = nb_filter_conv4*4  # Hidden dimension of linear layer
        self.reshape = (self.hidden_dim, 1, 1)  # Shape required to start transpose convs

        # Calculate dimensions of latent distribution
        self.latent_cont_dim = 0
        self.latent_disc_dim = 0
        self.num_disc_latents = 0
        if self.is_continuous:
            self.latent_cont_dim = self.latent_spec['cont']
        if self.is_discrete:
            self.latent_disc_dim += sum([dim for dim in self.latent_spec['disc']])
            self.num_disc_latents = len(self.latent_spec['disc'])
        self.latent_dim = self.latent_cont_dim + self.latent_disc_dim

        # parameters for conv filter number:
        self.nb_filter_conv1 = nb_filter_conv1
        self.nb_filter_conv2 = nb_filter_conv2
        self.nb_filter_conv3 = nb_filter_conv3
        self.nb_filter_conv4 = nb_filter_conv4

        # Define encoder layers
        encoder_layers = [
            nn.Conv2d(self.img_size[0], self.nb_filter_conv1, kernel_size=self.filter_size, stride=self.stride,
                      padding=1),  # B,self.nb_filter_conv1,32,32
            nn.ReLU()
        ]
        # Add additional layer if (64, 64) images
        if self.img_size[1:] == (64, 64):
            encoder_layers += [
                nn.Conv2d(self.nb_filter_conv1, self.nb_filter_conv2, kernel_size=self.filter_size, stride=self.stride,
                          padding=1),  # B, self.nb_filter_conv2, 16, 16
                nn.ReLU()
            ]
        elif self.img_size[1:] == (32, 32):
            # (32, 32) images are supported but do not require an extra layer
            pass
        else:
            raise RuntimeError("{} sized images not supported. Only (None, 32, 32) and (None, 64, 64) supported. "
                               "Build your own architecture or reshape images!".format(img_size))
        # Add final layers
        encoder_layers += [
            nn.Conv2d(self.nb_filter_conv2, self.nb_filter_conv3, kernel_size=self.filter_size, stride=self.stride,
                      padding=1),  # B, self.nb_filter_conv3, 8, 8
            nn.ReLU(),
            nn.Conv2d(self.nb_filter_conv3, self.nb_filter_conv4, kernel_size=self.filter_size, stride=self.stride,
                      padding=1),  # B, self.nb_filter_conv4, 4, 4
            nn.ReLU()
        ]

        if self.is_continuous:
            # Define encoder last continue conv
            last_conv_continue = [
                nn.Conv2d(self.nb_filter_conv4, self.nb_filter_conv4 * 2 * 2, kernel_size=self.filter_size),
                # B, self.nb_filter_conv4 * 2 * 2, 1, 1
                nn.ReLU()
            ]

        if self.is_discrete:
            # Define encoder last binary conv
            last_conv_binary = [
                nn.Conv2d(32, 32, kernel_size=self.filter_size, stride=self.stride, padding=1),
                StochasticBinaryActivation(estimator='ST')
            ]

        # Define encoder
        self.img_to_last_conv = nn.Sequential(*encoder_layers)
        if self.is_continuous:
            self.last_conv_to_continuous_features = nn.Sequential(*last_conv_continue)
        if self.is_discrete:
            self.last_conv_to_binary_features = nn.Sequential(*last_conv_binary)

        # Map encoded features into a hidden vector which will be used to
        # encode parameters of the latent distribution
        if self.is_continuous:
            self.features_to_hidden_continue = nn.Sequential(
                nn.Linear(self.nb_filter_conv4 * 2 * 2, self.latent_cont_dim * 2),
                nn.ReLU()
            )

        if self.is_discrete:
            self.features_to_hidden_binary = nn.Sequential(
                nn.Linear(self.nb_filter_conv4 * 2 * 2, self.latent_disc_dim),
                nn.ReLU()
            )

        # Encode parameters of latent distribution
        if self.is_discrete:
            # Linear layer for each of the categorical distributions
            fc_alphas = []
            for disc_dim in self.latent_spec['disc']:
                fc_alphas.append(nn.Linear(self.hidden_dim, disc_dim))
            self.fc_alphas = nn.ModuleList(fc_alphas)

        # Map latent samples to features to be used by generative model
        self.latent_to_features = nn.Sequential(
            nn.Linear(self.latent_dim, self.hidden_dim),  # B, 256
            nn.ReLU()
        )

        if self.is_classification:
            self.latent_to_classify = nn.Sequential(
                nn.Linear(self.latent_dim, self.hidden_dim),
                nn.ReLU(),
                nn.Linear(self.hidden_dim, self.nb_classes),
            )

        if self.is_classification_random_continue:
            self.latent_to_classify_random_continue = nn.Sequential(
                nn.Linear(self.latent_dim, self.hidden_dim),
                nn.ReLU(),
                nn.Linear(self.hidden_dim, self.nb_classes),
            )

        # Define decoder
        decoder_layers = []

        # Additional decoding layer for (64, 64) images
        decoder_layers += [
            nn.ConvTranspose2d(self.nb_filter_conv4 * 2 * 2, self.nb_filter_conv4, kernel_size=self.filter_size),  # B, self.nb_filter_conv4, 4, 4
            nn.ReLU(),
            nn.ConvTranspose2d(self.nb_filter_conv4, self.nb_filter_conv3, kernel_size=self.filter_size, stride=self.stride, padding=1),  # B, self.nb_filter_conv3, 8, 8
            nn.ReLU(),
            nn.ConvTranspose2d(self.nb_filter_conv3, self.nb_filter_conv2, kernel_size=self.filter_size, stride=self.stride, padding=1),  # B, self.nb_filter_conv2, 16, 16
            nn.ReLU()
        ]

        if self.img_size[1:] == (64, 64):
            decoder_layers += [
                nn.ConvTranspose2d(self.nb_filter_conv2, self.nb_filter_conv1, kernel_size=self.filter_size, stride=self.stride, padding=1),  # B, self.nb_filter_conv1, 32, 32
                nn.ReLU()
            ]

        decoder_layers += [
            nn.ConvTranspose2d(self.nb_filter_conv1, self.img_size[0], kernel_size=self.filter_size, stride=self.stride, padding=1),
            # B, img_size[0], 64, 64
            nn.Sigmoid()
        ]

        # Define decoder
        self.features_to_img = nn.Sequential(*decoder_layers)

    def encode(self, x):
        """
        Encodes an image into parameters of a latent distribution defined in
        self.latent_spec.
        Parameters
        ----------
        x : torch.Tensor
            Batch of data, shape (N, C, H, W)
        """
        batch_size = x.size()[0]

        # Encode image to hidden features
        if self.is_continuous:
            continuous_features = self.last_conv_to_continuous_features(self.img_to_last_conv(x))
            hidden_continue = self.features_to_hidden_continue(continuous_features.view(batch_size, -1))
        if self.is_discrete:
            binary_features = self.last_conv_to_binary_features(self.img_to_last_conv(x))
            hidden_binary = self.features_to_hidden_binary(binary_features.view(batch_size, -1))

        # Output parameters of latent distribution from hidden representation
        latent_dist = {}
        distribution_continue = hidden_continue
        mean = distribution_continue[:, :self.latent_cont_dim]
        logvar = distribution_continue[:, self.latent_cont_dim:]

        if self.is_continuous:
            latent_dist['cont'] = [mean, logvar]

        if self.is_discrete:
            latent_dist['disc'] = []
            for fc_alpha in self.fc_alphas:
                latent_dist['disc'].append(F.softmax(fc_alpha(hidden_binary), dim=1))

        return latent_dist

    def reparameterize(self, latent_dist):
        """
        Samples from latent distribution using the reparameterization trick.
        Parameters
        ----------
        latent_dist : dict
            Dict with keys 'cont' or 'disc' or both, containing the parameters
            of the latent distributions as torch.Tensor instances.
        """
        latent_sample = []
        latent_binary = []

        if self.is_continuous:
            mean, logvar = latent_dist['cont']
            cont_sample = self.sample_normal(mean, logvar)
            latent_sample.append(cont_sample)

        if self.is_discrete:
            for alpha in latent_dist['disc']:
                disc_sample = self.sample_gumbel_softmax(alpha)
                latent_sample.append(disc_sample)
                latent_binary.append(disc_sample)

        # Concatenate continuous and discrete samples into one large sample
        return torch.cat(latent_sample, dim=1), latent_binary

    def sample_normal(self, mean, logvar):
        """
        Samples from a normal distribution using the reparameterization trick.
        Parameters
        ----------
        mean : torch.Tensor
            Mean of the normal distribution. Shape (N, D) where D is dimension
            of distribution.
        logvar : torch.Tensor
            Diagonal log variance of the normal distribution. Shape (N, D)
        """
        if self.training:
            std = torch.exp(0.5 * logvar)
            eps = torch.zeros(std.size()).normal_()
            if torch.cuda.is_available():
                eps = eps.cuda()
            return mean + std * eps
        else:
            # Reconstruction mode
            return mean

    def sample_gumbel_softmax(self, alpha):
        """
        Samples from a gumbel-softmax distribution using the reparameterization
        trick.
        Parameters
        ----------
        alpha : torch.Tensor
            Parameters of the gumbel-softmax distribution. Shape (N, D)
        """
        if self.binary_variational:
            if self.training:
                # Sample from gumbel distribution
                unif = torch.rand(alpha.size())
                if torch.cuda.is_available():
                    unif = unif.cuda()
                gumbel = -torch.log(-torch.log(unif + EPS) + EPS)
                # Reparameterize to create gumbel softmax sample
                log_alpha = torch.log(alpha + EPS)
                logit = (log_alpha + gumbel) / self.temperature
                softmax = F.softmax(logit, dim=1)
                binary_result = torch.where(softmax > torch.mean(softmax), torch.tensor(1.0), torch.tensor(0.0))
                return binary_result
            else:
                # In reconstruction mode, pick most likely sample.log: remove temperature
                # Sample from gumbel distribution
                unif = torch.rand(alpha.size())
                if torch.cuda.is_available():
                    unif = unif.cuda()
                gumbel = -torch.log(-torch.log(unif + EPS) + EPS)
                # Reparameterize to create gumbel softmax sample
                log_alpha = torch.log(alpha + EPS)
                logit = (log_alpha + gumbel)
                softmax = F.softmax(logit, dim=1)
                binary_result = torch.where(softmax > torch.mean(softmax), torch.tensor(1.0), torch.tensor(0.0))
                return binary_result
        else:
            return alpha

    def decode(self, latent_sample):
        """
        Decodes sample from latent distribution into an image.
        Parameters
        ----------
        latent_sample : torch.Tensor
            Sample from latent distribution. Shape (N, L) where L is dimension
            of latent distribution.
        """
        features = self.latent_to_features(latent_sample)
        return self.features_to_img(features.view(-1, *self.reshape))

    def classification(self, latent_sample):
        """
        classify sample from latent distribution into an image.
        Parameters
        ----------
        latent_sample : torch.Tensor
            Sample from latent distribution. Shape (N, L) where L is dimension
            of latent distribution.
        """
        predicted = self.latent_to_classify(latent_sample)
        return F.log_softmax(predicted, dim=1)

    def classification_random_continue(self, latent_binary):
        """
        classify sample from latent distribution into an image.
        Parameters
        ----------
        latent_binary : torch.Tensor
            Sample from latent distribution. Shape (N, L) where L is dimension
            of latent distribution.
        """
        random_continue_latent = torch.randn(latent_binary[0].shape)
        latent_binary.append(random_continue_latent)
        latent_random_continue = torch.cat(latent_binary, dim=1)
        predicted_random_continue = self.latent_to_classify_random_continue(latent_random_continue)
        return F.log_softmax(predicted_random_continue, dim=1)

    def forward(self, x):
        """
        Forward pass of model.
        Parameters
        ----------
        x : torch.Tensor
            Batch of data. Shape (N, C, H, W)
        """
        latent_dist = self.encode(x)
        latent_sample, latent_binary = self.reparameterize(latent_dist)
        if self.is_classification:
            prediction = self.classification(latent_sample)
        if self.is_classification_random_continue:
            prediction_random_continue = self.classification_random_continue(latent_binary)
        output = self.decode(latent_sample)
        # print("\tIn Model: input size", x.size(), "output size", output.size())
        return output, latent_dist, prediction if 'prediction' in locals() else None, \
               prediction_random_continue if 'prediction_random_continue' in locals() else None
