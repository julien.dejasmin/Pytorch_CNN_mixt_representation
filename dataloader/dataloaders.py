import glob
import numpy as np
from skimage.io import imread
from torch.utils.data import Dataset, DataLoader, random_split
from torchvision import datasets, transforms
import os


# Load data
def load_dataset(dataset, batch_size, num_worker):
    if dataset == 'mnist':
        train_loader, test_loader = get_mnist_dataloaders(batch_size=batch_size)  # 10 classes
        img_size = (1, 32, 32)
        nb_classes = 10
        dataset_name = 'mnist'
        print('load dataset: {}, with: {} train images of shape: {}'.format(dataset_name,
                                                                            len(train_loader) * batch_size,
                                                                            img_size))

    elif dataset == 'fashion_data':
        train_loader, test_loader = get_fashion_mnist_dataloaders(batch_size=batch_size)  # 10 classes
        img_size = (1, 32, 32)
        nb_classes = 10
        dataset_name = 'fashion_data'
        print('load dataset: {}, with: {} train images of shape: {}'.format(dataset_name,
                                                                            len(train_loader) * batch_size,
                                                                            img_size))

    elif dataset == 'celeba_64':
        train_loader = get_celeba_dataloader(batch_size=batch_size)  # no classes for this dataset
        test_loader = train_loader
        img_size = (3, 64, 64)
        nb_classes = None
        dataset_name = 'celeba_64'
        print(
            'load dataset: {}, with: {} train images of shape: {}'.format(dataset_name,
                                                                          len(train_loader) * batch_size,
                                                                          img_size))
    elif dataset == 'rendered_chairs':
        train_loader, test_loader = get_chairs_dataloader(num_worker, batch_size=batch_size)  # 1393 classes
        img_size = (3, 64, 64)
        nb_classes = 1393
        dataset_name = 'rendered_chairs'
        print(
            'load dataset: {}, with: {} train images of shape: {}'.format(dataset_name,
                                                                          len(train_loader) * batch_size,
                                                                          img_size))

    elif dataset == 'dSprites':
        # dSprites is a dataset of 2D shapes procedurally generated from 6 ground truth independent latent factors.
        # These factors are color, shape, scale, rotation, x and y positions of a sprite.
        train_loader, test_loader = get_dsprites_dataloader(batch_size=batch_size)
        img_size = (1, 64, 64)
        nb_classes = 6
        dataset_name = 'dSprites'
        print(
            'load dataset: {}, with: {} train images of shape: {}'.format(dataset_name,
                                                                          len(train_loader) * batch_size,
                                                                          img_size))

    return train_loader, test_loader, dataset_name


def dataset_details(dataset):
    if dataset == 'mnist' or dataset == 'fashion_data':
        nb_classes = 10
        img_size = (1, 32, 32)
    elif dataset == 'celeba_64':
        nb_classes = None
        img_size = (3, 64, 64)
    elif dataset == 'rendered_chairs':
        nb_classes = 1393
        img_size = (3, 64, 64)
    elif dataset == 'dSprites':
        nb_classes = 6

    return nb_classes, img_size


def get_mnist_dataloaders(batch_size=128, path_to_data='../data/mnist'):
    """
    mnist dataloader with (28, 28) images.
    """
    all_transforms = transforms.Compose([
        transforms.Resize(32),
        transforms.ToTensor()
    ])
    train_data = datasets.MNIST(path_to_data, train=True, download=True,
                                transform=all_transforms)
    test_data = datasets.MNIST(path_to_data, train=False,
                               transform=all_transforms)
    train_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_data, batch_size=batch_size, shuffle=True)
    return train_loader, test_loader


def get_fashion_mnist_dataloaders(batch_size=128, path_to_data='../data/fashion_data'):
    """
    FashionMNIST dataloader with (28, 28) images.
    """
    all_transforms = transforms.Compose([
        transforms.Resize(32),
        transforms.ToTensor()
    ])
    train_data = datasets.FashionMNIST(path_to_data, train=True, download=True,
                                       transform=all_transforms)
    test_data = datasets.FashionMNIST(path_to_data, train=False,
                                      transform=all_transforms)
    train_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_data, batch_size=batch_size, shuffle=True)
    return train_loader, test_loader


def get_dsprites_dataloader(batch_size=128, path_to_data='../data/dSprites/dsprites.npz'):
    """
    DSprites dataloader.
    """
    dsprites_data = DSpritesDataset(path_to_data,
                                    transform=transforms.ToTensor())
    train_loader, test_loader = split_dataloader(dsprites_data, batch_size, percent_train=0.8)
    return train_loader, test_loader


def get_chairs_dataloader(num_worker=1, batch_size=128):
    """
    Chairs dataloader. Chairs are center cropped and resized to (64, 64).
    """
    path_to_data = 'data/rendered_chairs'
    if not os.path.exists(path_to_data):
        path_to_data = '../data/rendered_chairs'

    all_transforms = transforms.Compose([
        # transforms.Grayscale(),
        transforms.Resize(64),
        transforms.ToTensor()
    ])
    chairs_data = datasets.ImageFolder(root=path_to_data,
                                       transform=all_transforms)
    train_loader, test_loader = split_dataloader(chairs_data, batch_size, num_worker, percent_train=0.8)
    return train_loader, test_loader


def get_celeba_dataloader(batch_size=128, path_to_data='../data/celeba_64'):
    """
    CelebA dataloader with (64, 64) images.
    """
    all_transforms = transforms.Compose([
        # transforms.Resize(64),
        transforms.ToTensor()
    ])
    celeba_data = CelebADataset(path_to_data,
                                transform=all_transforms)
    celeba_loader = DataLoader(celeba_data, batch_size=batch_size,
                               shuffle=True)
    return celeba_loader


class DSpritesDataset(Dataset):
    """
    DSprites dataset.
    """

    def __init__(self, path_to_data, subsample=1, transform=None):
        """
        Parameters
        ----------
        subsample : int
            Only load every |subsample| number of images.
        """
        self.imgs = np.load(path_to_data)['imgs'][::subsample]
        self.transform = transform

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        # Each image in the dataset has binary values so multiply by 255 to get pixel values
        sample = self.imgs[idx] * 255
        # Add extra dimension to turn shape into (H, W) -> (H, W, C)
        sample = sample.reshape(sample.shape + (1,))

        if self.transform:
            sample = self.transform(sample)
        # Since there are no labels, we just return 0 for the "label" here
        return sample, 0


class CelebADataset(Dataset):
    """
    CelebA dataset with 64 by 64 images.
    """

    def __init__(self, path_to_data, subsample=1, transform=None):
        """
        Parameters
        ----------
        subsample : int
            Only load every |subsample| number of images.
        """
        self.img_paths = glob.glob(path_to_data + '/*')[::subsample]
        self.transform = transform

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, idx):
        sample_path = self.img_paths[idx]
        sample = imread(sample_path)

        if self.transform:
            sample = self.transform(sample)
        # Since there are no labels, we just return 0 for the "label" here
        return sample, 0


def split_dataloader(dataset, batch_size, num_worker, percent_train):
    """
    return dataset random split into train and test set
    :param percent_train: percent of data for train set
    :param batch_size:
    :param dataset:
    :return:
    """
    train_size = int(percent_train * len(dataset))
    test_size = len(dataset) - train_size
    train_data, test_data = random_split(dataset, [train_size, test_size])
    train_loader = DataLoader(train_data, batch_size=batch_size,
                              shuffle=True, num_workers=num_worker)
    test_loader = DataLoader(test_data, batch_size=batch_size,
                             shuffle=True, num_workers=num_worker)

    return train_loader, test_loader
