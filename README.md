# Pytorch_CNN_mixt_representation

In this repository we experiment VAE with mixes representation. 
For this we are inspired by the [github](https://github.com/Schlumberger/joint-vae) of pytorch implementation of [Learning Disentangled Joint Continuous and Discrete Representations](https://arxiv.org/pdf/1804.00104.pdf) (NIPS 2018).


# First part: experiments with simple VAE:

[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1lESx4d-IIA9rOxULwBD7e1aSrsa0gyPJ)


# References: 
* [1]: [Github: Schlumberger/joint-vae](https://github.com/Schlumberger/joint-vae).
