import json
import torch
from VAE_model.models import VAE


def load(path, expe_name):
    """
    Loads a trained model.
    Parameters
    ----------
    path : string
        Path to folder where model is saved. For example
        'trained_models/mnist/'. Note the path MUST end with a '/'
        :param expe_name:
    """
    path_to_specs = path + 'specs_' + expe_name + '.json'
    path_to_model = path + 'model_' + expe_name + '.pt'

    # Open specs file
    with open(path_to_specs) as specs_file:
        specs = json.load(specs_file)

    # Unpack specs
    dataset = specs["dataset"]
    # latent_spec
    latent_spec = {"cont": specs["latent_spec_cont"]}
    if specs["latent_spec_disc"] is not None:
        print("True")
        latent_spec["disc"] = specs["latent_spec_disc"]
    nb_classes = specs["nb_classes"]

    # Get image size
    if dataset == 'mnist' or dataset == 'fashion_data':
        img_size = (1, 32, 32)
    if dataset == 'dSprites':
        img_size = (1, 64, 64)
    if dataset == 'celeba_64' or dataset == 'rendered_chairs':
        img_size = (3, 64, 64)

    # Get model
    model = VAE(img_size=img_size, latent_spec=latent_spec, nb_classes=nb_classes)
    model.load_state_dict(torch.load(path_to_model,
                                     map_location=lambda storage, loc: storage))

    return model
