import imageio
import numpy as np
import torch
from torch.nn import functional as F
import time
import datetime
import os

EPS = 1e-12


class Trainer:
    def __init__(self, model, device, optimizer, criterion, save_step, ckpt_dir, load_model_checkpoint, ckpt_name,
                 expe_name, dataset, normalize_loss_by_pixel=True, cont_capacity=None, disc_capacity=None,
                 is_beta=False, beta=None,
                 print_loss_every=50, record_loss_every=5):

        """
        Class to handle training of model.
        Parameters
        ----------
        model : VAE_model.models.VAE instance
        optimizer : torch.optim.Optimizer instance
        cont_capacity : tuple (float, float, int, float) or None
            Tuple containing (min_capacity, max_capacity, num_iters, gamma_z).
            Parameters to control the capacity of the continuous latent
            channels. Cannot be None if model.is_continuous is True.
        disc_capacity : tuple (float, float, int, float) or None
            Tuple containing (min_capacity, max_capacity, num_iters, gamma_c).
            Parameters to control the capacity of the discrete latent channels.
            Cannot be None if model.is_discrete is True.
        print_loss_every : int
            Frequency with which loss is printed during training.
        record_loss_every : int
            Frequency with which loss is recorded during training.
        """
        if 'parallel' in str(type(model)):
            self.model = model.module
        else:
            self.model = model
        self.device = device
        self.optimizer = optimizer
        self.criterion = criterion
        self.cont_capacity = cont_capacity
        self.disc_capacity = disc_capacity
        self.is_beta = is_beta
        self.beta = beta
        self.print_loss_every = print_loss_every
        self.record_loss_every = record_loss_every
        self.load_model_checkpoint = load_model_checkpoint
        self.normalize_loss_by_pixel = normalize_loss_by_pixel

        if self.model.is_continuous and self.cont_capacity is None:
            # raise RuntimeError("Model is continuous but cont_capacity not provided.")
            print("don't use continuous capacity")

        if self.model.is_discrete and self.disc_capacity is None:
            raise RuntimeError("Model is discrete but disc_capacity not provided.")

        # Initialize attributes
        self.num_steps = 0
        self.batch_size = None
        self.losses = {'loss': [],
                       'recon_loss': [],
                       'kl_loss': []}
        self.global_iter = 0
        self.mean_epoch_loss = []
        self.reconstruction_loss = []
        self.kl_loss = []
        self.save_step = save_step
        self.expe_name = expe_name
        self.ckpt_dir = ckpt_dir
        self.dataset = dataset
        self.ckpt_dir = os.path.join('trained_models', self.dataset, self.expe_name, self.ckpt_dir)
        self.ckpt_name = ckpt_name
        if self.ckpt_name is not None:
            self.load_checkpoint(self.ckpt_name)

        # Keep track of divergence values for each latent variable
        if self.model.is_continuous:
            self.losses['kl_loss_cont'] = []
            # For every dimension of continuous latent variables
            for i in range(self.model.latent_spec['cont']):
                self.losses['kl_loss_cont_' + str(i)] = []

        if self.model.is_discrete:
            self.losses['kl_loss_disc'] = []
            # For every discrete latent variable
            for i in range(len(self.model.latent_spec['disc'])):
                self.losses['kl_loss_disc_' + str(i)] = []

        if self.model.is_continuous:
            self.losses['classification_loss'] = []

        if self.model.is_classification_random_continue:
            self.losses['classification_continue_random_loss'] = []

    def train(self, data_loader, epochs=10, save_training_gif=None):
        """
        Trains the model.
        Parameters
        ----------
        data_loader : torch.utils.data.DataLoader
        epochs : int
            Number of epochs to train the model for.
        save_training_gif : None or tuple (string, Visualizer instance)
            If not None, will use visualizer object to create image of samples
            after every epoch and will save gif of these at location specified
            by string. Note that string should end with '.gif'.
        """
        if save_training_gif is not None:
            training_progress_images = []

        self.batch_size = data_loader.batch_size
        self.model.train()
        for epoch in range(epochs):
            self.global_iter += 1
            mean_epoch_loss, recon_loss, kl_loss, prediction_loss, prediction_random_continue_loss = self._train_epoch(data_loader)
            mean_epoch_loss_pixels = self.batch_size * self.model.num_pixels * mean_epoch_loss
            mean_epoch_loss_pixels_recon_loss = self.batch_size * self.model.num_pixels * recon_loss
            mean_epoch_loss_pixels_kl_loss = self.batch_size * self.model.num_pixels * kl_loss
            mean_epoch_loss_pixels_prediction_loss = self.batch_size * self.model.num_pixels * prediction_loss
            mean_epoch_loss_pixels_prediction_random_continue_loss = self.batch_size * self.model.num_pixels * prediction_random_continue_loss
            total_loss = mean_epoch_loss_pixels_recon_loss + mean_epoch_loss_pixels_kl_loss + mean_epoch_loss_pixels_prediction_loss + mean_epoch_loss_pixels_prediction_random_continue_loss
            
            print('Epoch: {} Average loss: {:.2f}'.format(epoch + 1, mean_epoch_loss_pixels))

            if self.global_iter % self.save_step == 0:
                self.save_checkpoint('last')

            if save_training_gif is not None:
                # Generate batch of images and convert to grid
                viz = save_training_gif[1]
                viz.save_images = False
                img_grid = viz.all_latent_traversals(size=10)
                # Convert to numpy and transpose axes to fit imageio convention
                # i.e. (width, height, channels)
                img_grid = np.transpose(img_grid.numpy(), (1, 2, 0))
                # Add image grid to training progress
                training_progress_images.append(img_grid)
                
            
            # Record losses
            if self.model.training and self.num_steps % self.record_loss_every == 1:
                self.mean_epoch_loss.append(mean_epoch_loss_pixels)
                self.losses['recon_loss'].append(mean_epoch_loss_pixels_recon_loss.item())
                self.losses['kl_loss'].append(mean_epoch_loss_pixels_kl_loss.item())
                self.losses['loss'].append(total_loss.item())
                if self.model.is_classification:
                    self.losses['classification_loss'].append(mean_epoch_loss_pixels_prediction_loss.item())
                if self.model.is_classification_random_continue:
                    self.losses['classification_continue_random_loss'].append(mean_epoch_loss_pixels_prediction_random_continue_loss.item())

        if save_training_gif is not None:
            imageio.mimsave(save_training_gif[0], training_progress_images,
                            fps=24)

    def _train_epoch(self, data_loader):
        """
        Trains the model for one epoch.
        Parameters
        ----------
        data_loader : torch.utils.data.DataLoader
        """
        epoch_loss = 0.
        epoch_recon_loss = 0.
        epoch_kl_loss = 0.
        epoch_pred_loss = 0.
        epoch_pred_random_loss = 0.
        
        print_every_loss = 0.  # Keeps track of loss to print every self.print_loss_every

        epoch_start = time.time()
        for batch_idx, (data, labels) in enumerate(data_loader):
            start = time.time()

            label = labels.to(self.device)
            data = data.to(self.device)
            iter_loss, recon_loss_iter, kl_loss_iter, pred_loss_iter, pred_random_loss_iter = self._train_iteration(data, label)
            
            epoch_loss += iter_loss
            epoch_recon_loss += recon_loss_iter
            epoch_kl_loss += kl_loss_iter
            epoch_pred_loss += pred_loss_iter
            epoch_pred_random_loss += pred_random_loss_iter
            
            print_every_loss += iter_loss

            batch_time = time.time() - start

            # Print loss info every self.print_loss_every iteration
            if batch_idx % self.print_loss_every == 0:
                if batch_idx == 0:
                    mean_loss = print_every_loss
                else:
                    mean_loss = print_every_loss / self.print_loss_every
                print('{}/{}\tLoss: {:.3f}'.format(batch_idx * len(data),
                                                   len(data_loader.dataset),
                                                   self.model.num_pixels * mean_loss))
                print_every_loss = 0.

        elapse_time = time.time() - epoch_start
        elapse_time = datetime.timedelta(seconds=elapse_time)
        print("Training time {}".format(elapse_time))

        # Return mean epoch loss
        return epoch_loss/len(data_loader.dataset), epoch_recon_loss/len(data_loader.dataset), epoch_kl_loss/len(data_loader.dataset), epoch_pred_loss/len(data_loader.dataset), epoch_pred_random_loss/len(data_loader.dataset)

    def _train_iteration(self, data, label):
        """
        Trains the model for one iteration on a batch of data.
        Parameters
        ----------
        data : torch.Tensor
            A batch of data. Shape (N, C, H, W)
        """
        self.num_steps += 1

        self.optimizer.zero_grad()
        recon_batch, latent_dist, prediction, prediction_random_continue = self.model(data)
        # print("Outside: input size", input.size(), "output_size", recon_batch.size())
        loss, recon_loss, kl_loss, pred_loss, pred_random_loss = self._loss_function(data, label, recon_batch, latent_dist, prediction, prediction_random_continue)
        loss.backward()
        self.optimizer.step()

        train_loss = loss.item()
        recon_loss_iter = recon_loss
        kl_loss_iter = kl_loss
        pred_loss_iter = pred_loss
        pred_random_loss_iter = pred_random_loss
        
        return train_loss, recon_loss_iter, kl_loss_iter, pred_loss_iter, pred_random_loss_iter

    def _loss_function(self, data, label, recon_data, latent_dist, prediction, prediction_random_continue):
        """
        Calculates loss for a batch of data.
        Parameters
        ----------
        data : torch.Tensor
            Input data (e.g. batch of images). Should have shape (N, C, H, W)
        recon_data : torch.Tensor
            Reconstructed data. Should have shape (N, C, H, W)
        latent_dist : dict
            Dict with keys 'cont' or 'disc' or both containing the parameters
            of the latent distributions as values.
        prediction : torch.tensor
            Last fc output for classification of cat(e1, e2). Should have shape (N, nb_classes)
        prediction_random_continue : torch.tensor
            Last fc output for classification of cat(e1_random, e2). Should have shape (N, nb_classes)

        """
        # Reconstruction loss is pixel wise cross-entropy
        # recon_loss = F.binary_cross_entropy(recon_data.view(-1, self.model.num_pixels),
        #                                    data.view(-1, self.model.num_pixels))
        # F.binary_cross_entropy takes mean over pixels, so unnormalise this
        # recon_loss *= self.model.num_pixels

        # with mse loss:
        recon_loss = F.mse_loss(recon_data, data, size_average=False).div(self.batch_size)
        # self.reconstruction_loss.append(recon_loss)

        prediction_loss = 0
        prediction_random_continue_loss = 0
        if self.model.is_classification:
            prediction_loss = self.criterion(prediction, label)
        if self.model.is_classification_random_continue:
            prediction_random_continue_loss = self.criterion(prediction_random_continue, label)

        # Calculate KL divergences
        kl_cont_loss = 0  # Used to compute capacity loss (but not a loss in itself)
        kl_disc_loss = 0  # Used to compute capacity loss (but not a loss in itself)
        cont_capacity_loss = 0
        disc_capacity_loss = 0

        if self.model.is_continuous:
            # Calculate KL divergence
            mean, logvar = latent_dist['cont']
            kl_cont_loss = self._kl_normal_loss(mean, logvar)
            if self.cont_capacity is not None:
                # Linearly increase capacity of continuous channels
                cont_min, cont_max, cont_num_iters, cont_gamma = \
                    self.cont_capacity
                # Increase continuous capacity without exceeding cont_max
                cont_cap_current = (cont_max - cont_min) * self.num_steps / float(cont_num_iters) + cont_min
                cont_cap_current = min(cont_cap_current, cont_max)
                # Calculate continuous capacity loss
                cont_capacity_loss = cont_gamma * torch.abs(cont_cap_current - kl_cont_loss)

        if self.model.is_discrete:
            # Calculate KL divergence
            kl_disc_loss = self._kl_multiple_discrete_loss(latent_dist['disc'])
            # Linearly increase capacity of discrete channels
            disc_min, disc_max, disc_num_iters, disc_gamma = \
                self.disc_capacity
            if self.disc_capacity is not None:
                # Increase discrete capacity without exceeding disc_max or theoretical
                # maximum (i.e. sum of log of dimension of each discrete variable)
                disc_cap_current = (disc_max - disc_min) * self.num_steps / float(disc_num_iters) + disc_min
                disc_cap_current = min(disc_cap_current, disc_max)
                # Require float conversion here to not end up with numpy float
                disc_theoretical_max = sum([float(np.log(disc_dim)) for disc_dim in self.model.latent_spec['disc']])
                disc_cap_current = min(disc_cap_current, disc_theoretical_max)
                # Calculate discrete capacity loss
                disc_capacity_loss = disc_gamma * torch.abs(disc_cap_current - kl_disc_loss)

        # Calculate total kl value to record it
        kl_loss = kl_cont_loss + kl_disc_loss
        self.kl_loss.append(kl_loss)

        # Calculate total loss
        if self.is_beta:
            total_loss = recon_loss + self.beta * kl_cont_loss
        elif self.cont_capacity is None:
            total_loss = recon_loss + kl_loss + prediction_loss + prediction_random_continue_loss
        else:
            total_loss = recon_loss + cont_capacity_loss + disc_capacity_loss + prediction_loss + \
                         prediction_random_continue_loss

        # To avoid large losses normalise by number of pixels:
        if self.normalize_loss_by_pixel:
            return total_loss/self.model.num_pixels, recon_loss/self.model.num_pixels, kl_loss/self.model.num_pixels, prediction_loss/self.model.num_pixels, prediction_random_continue_loss/self.model.num_pixels
        else:
            return total_loss, recon_loss, kl_loss, prediction_loss, prediction_random_continue_loss

    def _kl_normal_loss(self, mean, logvar):
        """
        Calculates the KL divergence between a normal distribution with
        diagonal covariance and a unit normal distribution.
        Parameters
        ----------
        mean : torch.Tensor
            Mean of the normal distribution. Shape (N, D) where D is dimension
            of distribution.
        logvar : torch.Tensor
            Diagonal log variance of the normal distribution. Shape (N, D)
        """
        # Calculate KL divergence
        kl_values = -0.5 * (1 + logvar - mean.pow(2) - logvar.exp())
        # Mean KL divergence across batch for each latent variable
        kl_means = torch.mean(kl_values, dim=0)
        # KL loss is sum of mean KL of each latent variable
        kl_loss = torch.sum(kl_means)

        # Record losses
        if self.model.training and self.num_steps % self.record_loss_every == 1:
            self.losses['kl_loss_cont'].append(kl_loss.item())
            for i in range(self.model.latent_spec['cont']):
                self.losses['kl_loss_cont_' + str(i)].append(kl_means[i].item())

        return kl_loss

    def _kl_multiple_discrete_loss(self, alphas):
        """
        Calculates the KL divergence between a set of categorical distributions
        and a set of uniform categorical distributions.
        Parameters
        ----------
        alphas : list
            List of the alpha parameters of a categorical (or gumbel-softmax)
            distribution. For example, if the categorical atent distribution of
            the model has dimensions [2, 5, 10] then alphas will contain 3
            torch.Tensor instances with the parameters for each of
            the distributions. Each of these will have shape (N, D).
        """
        # Calculate kl losses for each discrete latent
        kl_losses = [self._kl_discrete_loss(alpha) for alpha in alphas]

        # Total loss is sum of kl loss for each discrete latent
        kl_loss = torch.sum(torch.cat(kl_losses))

        # Record losses
        if self.model.training and self.num_steps % self.record_loss_every == 1:
            self.losses['kl_loss_disc'].append(kl_loss.item())
            for i in range(len(alphas)):
                self.losses['kl_loss_disc_' + str(i)].append(kl_losses[i].item())

        return kl_loss

    def _kl_discrete_loss(self, alpha):
        """
        Calculates the KL divergence between a categorical distribution and a
        uniform categorical distribution.
        Parameters
        ----------
        alpha : torch.Tensor
            Parameters of the categorical or gumbel-softmax distribution.
            Shape (N, D)
        """
        disc_dim = int(alpha.size()[-1])
        log_dim = torch.Tensor([np.log(disc_dim)])
        if self.use_cuda:
            log_dim = log_dim.cuda()
        # Calculate negative entropy of each row
        neg_entropy = torch.sum(alpha * torch.log(alpha + EPS), dim=1)
        # Take mean of negative entropy across batch
        mean_neg_entropy = torch.mean(neg_entropy, dim=0)
        # KL loss of alpha with uniform categorical variable
        kl_loss = log_dim + mean_neg_entropy
        return kl_loss

    def save_checkpoint(self, filename, silent=False):

        model_states = {'model': self.model.state_dict(), }
        optim_states = {'optim': self.optimizer.state_dict(), }
        states = {'loss': self.mean_epoch_loss,
                  'recons_loss': self.reconstruction_loss,
                  'kl_loss': self.kl_loss,
                  'iter': self.global_iter,
                  'model_states': model_states,
                  'optim_states': optim_states}

        file_path = os.path.join(self.ckpt_dir, filename)
        with open(file_path, mode='wb+') as f:
            torch.save(states, f)
        if not silent:
            print("=> saved checkpoint '{}' (iter {})".format(file_path, self.global_iter))

    def load_checkpoint(self, filename):
        file_path = os.path.join(self.ckpt_dir, filename)
        if os.path.isfile(file_path):
            checkpoint = torch.load(file_path)
            self.mean_epoch_loss = checkpoint['loss']
            if self.load_model_checkpoint:
                self.reconstruction_loss = checkpoint['recons_loss']
                self.kl_loss = checkpoint['kl_loss']
            self.global_iter = checkpoint['iter']
            self.model.load_state_dict(checkpoint['model_states']['model'])
            self.optimizer.load_state_dict(checkpoint['optim_states']['optim'])
            print("=> loaded checkpoint '{} (iter {})'".format(file_path, self.global_iter))
        else:
            print("=> no checkpoint found at '{}'".format(file_path))


def gpu_config(model):
    use_gpu = torch.cuda.is_available()
    gpu_count = torch.cuda.device_count()
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    if use_gpu:
        if gpu_count > 1:
            print('use {} gpu who named:'.format(gpu_count))
            for i in range(gpu_count):
                print(torch.cuda.get_device_name(i))
            model = torch.nn.DataParallel(model, device_ids=[device_id for device_id in range(gpu_count)])
        else:
            print('use 1 gpu who named: {}'.format(torch.cuda.get_device_name(0)))
    else:
        print('no gpu available !')
    model.to(device)
    return model, use_gpu, device
